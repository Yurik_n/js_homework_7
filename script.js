/*
1)Опишіть своїми словами як працює метод forEach.
2)Як очистити масив?
3)Як можна перевірити, що та чи інша змінна є масивом?


1) forEach - метод масиву який перебирає масив, тобто проходить через кожен елемент і для кожного елементу викликає функцію,
 яку предамо параметром в цей метод forEach(параметр)
2)  Щоб очистити масив достатньо властивості length присвоїти нуль - array.length = 0 
3) Array.isArray(arr) - для перевірки масиву arr, поверне true якщо arr є масивом

*/
const array = [1, "er", null,  4, 5, 6, 7, "rtr", "rtrty", 'Apple', { name: "John", lastName: "Smith" }, true]


let filterBy = (array, valueType) => {
    let newArray = []
for (const key of array) {
    if (typeof(key) !== typeof(valueType)) {
        newArray.push(key)      
    }
}
return newArray
} 


console.log(array)
console.log(filterBy(array, "text"))

